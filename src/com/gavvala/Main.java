package com.gavvala;

import java.math.BigInteger;

public class Main {
    public static void main(String[] args) {

        System.out.println(factorial( 20));
        //System.out.println(factorialGt20( 450000));
    }

    static long factorial(int n){
        if(n==0){
            return 1;
        }else{
            return (n * factorial( n-1 ));
        }
    }

    public static BigInteger factorialGt20(int n){
        BigInteger result = BigInteger.ONE;
        for(int i=2;i<=n;i++){
            result = result.multiply( BigInteger.valueOf( i ) );
        }
        return result;
    }
}
